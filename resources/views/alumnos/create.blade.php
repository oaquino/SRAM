@extends('home')

@section('content_header')
    <div class="container">
        <h1>Crear alumno</h1>
    </div>
@stop

@section('content')
<div class="container">
    <form method="post" action="{{ route('alumnos.store') }}">
        @csrf
        <div class="row mb-3">
            <div class="col-md-4">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control text-uppercase" name="nombre" id="nombre" placeholder="Nombre" maxlength="30" required value="{{ old('nombre') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="primer_apellido">Primer apellido:</label>
                <input type="text" class="form-control text-uppercase" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido" maxlength="30" required value="{{ old('primer_apellido') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="segundo_apellido">Segundo apellido:</label>
                <input type="text" class="form-control text-uppercase" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido" maxlength="30" required value="{{ old('segundo_apellido') }}" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <label for="sexo">Sexo:</label>
                <select class="selectpicker form-control text-uppercase" aria-label="Default select example" name="sexo" id="sexo" required value="{{ old('sexo') }}">
                    <option selected>Selecciona uno</option>
                    <option value="F">Femenino</option>
                    <option value="M">Masculino</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="edad">Edad:</label>
                <input type="text" name="edad" id="edad" class="form-control text-uppercase" maxlength="2" placeholder="Edad" required value="{{ old('edad') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="anio_escolar">Año escolar:</label>
                <input type="text" name="anio_escolar" id="anio_escolar" class="form-control text-uppercase" maxlength="20" placeholder="Ejem: Primero, Segundo" required value="{{ old('anio_escolar') }}" autocomplete="off">
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-4">
                <label for="nombre_tutor">Nombre tutor:</label>
                <input type="text" name="nombre_tutor" id="nombre_tutor" class="form-control text-uppercase" maxlength="90" placeholder="Nombre tutor" required value="{{ old('nombre_tutor') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="telefono_tutor">Teléfono tutor:</label>
                <input type="text" name="telefono_tutor" id="telefono_tutor" class="form-control text-uppercase" maxlength="10" placeholder="Teléfono tutor" required value="{{ old('telefono_tutor') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="materia">Materia:</label>
                <select class="selectpicker form-control text-uppercase" multiple name="materia[]" id="materia" placeholder="SELECCIONE UNA">
                    @foreach($maestros as $maestro)
                        <option value="{{$maestro->id}}">{{$maestro->materia}}-{{$maestro->nombre}} {{$maestro->primer_apellido}}
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-11">
                <input type="submit" class="btn btn-primary float-right" value="Guardar">
            </div>
            <div class="col-1">
                <a class="btn btn-danger float-end" href="{{route('alumnos.index')}}">Cancelar</a>
            </div>
        </div>
    </form>
</div>
@include('sweetalert::alert')
@stop

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@stop


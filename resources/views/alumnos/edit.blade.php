@extends('home')

@section('content_header')
    <div class="container">
        <h1>Editar alumno</h1>
    </div>
@stop

@section('content')

<div class="container">
    <form method="post" action="{{route('alumnos.update', ['alumno' => $alumno->id])}}">
        @csrf
        @method('PUT')
        <div class="row mb-3">
            <div class="col-md-4">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control text-uppercase" name="nombre" id="nombre" placeholder="Nombre" maxlength="30" value="{{$alumno->nombre}}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="primer_apellido">Primer apellido:</label>
                <input type="text" class="form-control text-uppercase" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido" maxlength="30" value="{{$alumno->primer_apellido}}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="segundo_apellido">Segundo apellido:</label>
                <input type="text" class="form-control text-uppercase" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido" maxlength="30" value="{{$alumno->segundo_apellido}}" autocomplete="off">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-md-4">
                <label for="sexo">Sexo:</label>
                <select class="selectpicker form-control text-uppercase" name="sexo" id="sexo">
                    <option value="F" @if($alumno->sexo == 'F') selected @endif>Femenino</option>
                    <option value="M" @if($alumno->sexo == 'M') selected @endif>Masculino</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="edad">Edad:</label>
                <input type="text" name="edad" id="edad" class="form-control text-uppercase" maxlength="2" value="{{$alumno->edad}}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="anio_escolar">Año escolar:</label>
                <input type="text" name="anio_escolar" id="anio_escolar" class="form-control text-uppercase" maxlength="20" value="{{$alumno->anio_escolar}}" autocomplete="off">
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-4">
                <label for="nombre_tutor">Nombre tutor:</label>
                <input type="text" name="nombre_tutor" id="nombre_tutor" class="form-control text-uppercase" maxlength="90" value="{{$alumno->nombre_tutor}}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="telefono_tutor">Teléfono tutor:</label>
                <input type="text" name="telefono_tutor" id="telefono_tutor" class="form-control text-uppercase" maxlength="10" value="{{$alumno->telefono_tutor}}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="materia">Materia:</label>
                <select class="selectpicker form-control text-uppercase" multiple name="materia[]" id="materia" placeholder="SELECCIONE UNA MATERIA">
                    @foreach($maestros as $maestro)
                        <option value="{{$maestro->id}}" @if(in_array($maestro->id, $asignados)) selected @endif>{{$maestro->materia}}-{{$maestro->nombre}} {{$maestro->primer_apellido}}
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-11">
                <input type="submit" class="btn btn-primary float-right" value="Guardar">
            </div>
            <div class="col-1">
                <a class="btn btn-danger float-end" href="{{route('alumnos.index')}}">Cancelar</a>
            </div>
        </div>
    </form>
</div>
@include('sweetalert::alert')
@stop
@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

@stop

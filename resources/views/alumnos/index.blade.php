@extends('home')

@section('content_header')
    <div class="container">
        <h1>Mostrar alumnos</h1>
    </div>
@stop

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
                <a class="btn btn-primary float-end" href="{{route('alumnos.create')}}">Nuevo alumno</a>
        </div>
    </div>
    <table class="table table-dark table-striped">
        <tr>
            <th>Nombre</th>
            <th>Sexo</th>
            <th>Edad</th>
            <th>Año escolar</th>
            <th>Nombre tutor</th>
            <th>Teléfono tutor</th>
            <!-- <th>Materia</th> -->
            <th colspan=3>Acción</th>
        </tr> 
        @foreach($alumnos as $alumno)
            <tr>
                <td>{{$alumno->nombre}} {{$alumno->primer_apellido}} {{$alumno->segundo_apellido}}</td>
                <td>{{$alumno->sexo}}</td>
                <td>{{$alumno->edad}}</td>
                <td>{{$alumno->anio_escolar}}</td>
                <td>{{$alumno->nombre_tutor}}</td>
                <td>{{$alumno->telefono_tutor}}</td>
                <td>
                    <a href="{{url('/alumnos/'.$alumno->id.'/edit')}}" class="btn btn-primary"><i class="fas fa-solid fa-pen" title="Editar"></i></a>
                </td>
                <td>
                    <a href="{{route('alumnos.show', $alumno->id)}}" class="btn btn-primary"><i class="fas fa-solid fa-eye" title="Mostrar"></i></a>
                </td>
                <td>
                    <form method="post" action="{{route('alumnos.destroy', $alumno)}}" id="formDestroy{{$alumno->id}}" onsubmit="borrar(event)">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-primary"><i class="fas fa-solid fa-ban" title="Eliminar"></i></button>
                    </form>
                </td>
            </tr>   
        @endforeach
    </table>
    @include('sweetalert::alert')
@stop

@section('js')
<script type="text/javascript" src="/js/app.js"></script>

<script>
    function borrar(e){
        let formD = e.target.id
        e.preventDefault();
        Swal.fire({
            title: '¿Está seguro que desea borrar al alumno?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, borrar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                document.forms[formD].submit();
            }
        })
    }
</script>
@stop
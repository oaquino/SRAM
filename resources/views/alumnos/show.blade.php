@extends('home')

@section('content_header')
    <div class="container">
        <h1>Mostrar alumno</h1>
    </div>
@stop

@section('content')

<div class="container">
    <div class="row mb-3">
        <div class="col-md-4">
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" maxlength="30" value="{{$alumno->nombre}}" readonly>
        </div>
        <div class="col-md-4">
            <label for="primer_apellido">Primer apellido:</label>
            <input type="text" class="form-control" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido" maxlength="30" value="{{$alumno->primer_apellido}}" readonly>
        </div>
        <div class="col-md-4">
            <label for="segundo_apellido">Segundo apellido:</label>
            <input type="text" class="form-control" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido" maxlength="30" value="{{$alumno->segundo_apellido}}" readonly>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-md-4">
            <label for="sexo">Sexo:</label>
            <select class="form-select" aria-label="Default select example" name="sexo" id="sexo" disabled>
                <option value="F" @if($alumno->sexo == 'F') selected @endif>Femenino</option>
                <option value="M" @if($alumno->sexo == 'M') selected @endif>Masculino</option>
            </select>
        </div>
        <div class="col-md-4">
            <label for="edad">Edad:</label>
            <input type="text" name="edad" id="edad" class="form-control" maxlength="2" value="{{$alumno->edad}}" readonly>
        </div>
        <div class="col-md-4">
            <label for="anio_escolar">Año escolar:</label>
            <input type="text" name="anio_escolar" id="anio_escolar" class="form-control" maxlength="2" value="{{$alumno->anio_escolar}}" readonly>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-4">
            <label for="nombre_tutor">Nombre tutor:</label>
            <input type="text" name="nombre_tutor" id="nombre_tutor" class="form-control" maxlength="90" value="{{$alumno->nombre_tutor}}" readonly>
        </div>
        <div class="col-md-4">
            <label for="telefono_tutor">Teléfono tutor:</label>
            <input type="text" name="telefono_tutor" id="telefono_tutor" class="form-control" maxlength="10" value="{{$alumno->telefono_tutor}}" readonly>
        </div>
        <div class="col-md-4">
            <label for="materia">Materia:</label>
            <select class="form-select" aria-label="Default select example" name="materia" id="materia" disabled>
                <option selected>SIN ASIGNAR</option>
                @foreach($maestros as $maestro)
                    <option value="{{$maestro->id}}" @if(in_array($maestro->id, $asignados)) selected @endif>{{$maestro->materia}}-{{$maestro->nombre}} {{$maestro->primer_apellido}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <a class="btn btn-danger float-end" href="{{route('alumnos.index')}}">Regresar</a>
        </div>
    </div>
</div>

@stop


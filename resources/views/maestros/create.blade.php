@extends('home')

@section('content_header')
    <div class="container">
        <h1>Crear maestro</h1>
    </div>
@stop

@section('content')

<div class="container">
    <form method="post" action="{{route('maestros.store')}}">
        @csrf
        <div class="row mb-3">
            <div class="col-md-4">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control text-uppercase" name="nombre" id="nombre" placeholder="Nombre" maxlength="30" required value="{{ old('nombre') }}" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="primer_apellido">Primer apellido:</label>
                <input type="text" class="form-control text-uppercase" name="primer_apellido" id="primer_apellido" placeholder="Primer apellido" maxlength="30" required value="{{ old('primer_apellido') }}" autocomplete="off" >
            </div>
            <div class="col-md-4">
                <label for="segundo_apellido">Segundo apellido:</label>
                <input type="text" class="form-control text-uppercase" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo apellido" maxlength="30" required value="{{ old('segundo_apellido') }}" autocomplete="off">
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-4">
                <label for="sexo">Sexo:</label>
                <select class="form-select text-uppercase" aria-label="Default select example" name="sexo" id="sexo" required value="{{ old('sexo') }}">
                    <option selected>Selecciona uno</option>
                    <option value="F">Femenino</option>
                    <option value="M">Masculino</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="edad">Edad:</label>
                <input type="text" name="edad" id="edad" class="form-control text-uppercase" maxlength="2" required value="{{ old('edad') }}" placeholder="Edad" autocomplete="off">
            </div>
            <div class="col-md-4">
                <label for="materia">Materia:</label>
                <input type="text" name="materia" id="materia" class="form-control text-uppercase" maxlength="20" required value="{{ old('materia') }}" placeholder="Materia" autocomplete="off">
            </div>
        </div>
        <div class="row">
            <div class="col-11">
                <input type="submit" class="btn btn-primary float-end" value="Guardar">
            </div>
            <div class="col-1">
                <a class="btn btn-danger float-end" href="{{route('maestros.index')}}">Cancelar</a>
            </div>
        </div>
    </form>
</div>
@include('sweetalert::alert')

@stop

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@stop
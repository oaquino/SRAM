@extends('home')

@section('content_header')
    <div class="container">
        <h1>Mostrar maestros</h1>
    </div>
@stop

@section('content')
    <div class="row mb-2">
        <div class="col-md-12">
                <a class="btn btn-primary float-end" href="{{route('maestros.create')}}">Nuevo maestro</a>
        </div>
    </div>
    <table class="table table-dark table-striped">
        <tr>
            <th>Nombre</th>
            <th>Sexo</th>
            <th>Edad</th>
            <th>Materia</th>
            <th colspan="3">Acción</th>
        </tr> 
            @foreach($maestros as $maestro)
            <tr>
                <td>{{$maestro->nombre}} {{$maestro->primer_apellido}} {{$maestro->segundo_apellido}}</td>
                <td>{{$maestro->sexo}}</td>
                <td>{{$maestro->edad}}</td>
                <td>{{$maestro->materia}}</td>
                <td>
                    <a href="{{url('/maestros/'.$maestro->id.'/edit')}}" class="btn btn-primary"><i class="fas fa-solid fa-pen" title="Editar"></i></a>
                </td>
                <td>
                    <a href="{{route('maestros.show', $maestro->id)}}" class="btn btn-primary"><i class="fas fa-solid fa-eye" title="Mostrar"></i></a>
                </td>
                <td>
                    <form method="post" action="{{route('maestros.destroy', $maestro->id)}}" id="formDestroy{{$maestro->id}}" onsubmit="borrar(event)">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-primary"><i class="fas fa-solid fa-ban" title="Eliminar"></i></button>
                    </form>
                </td>
            </tr>   
            @endforeach
    </table>
    @include('sweetalert::alert')

@stop

@section('js')
<script type="text/javascript" src="/js/app.js"></script>

<script>
    function borrar(e){
        let formD = e.target.id
        e.preventDefault();
        Swal.fire({
            title: '¿Está seguro que desea borrar al maestro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '¡Sí, borrar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.isConfirmed) {
                document.forms[formD].submit();
            }
        })
    }
</script>
@stop


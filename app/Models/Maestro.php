<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Maestro extends Model
{
    protected $fillable = [
        'nombre',
        'primer_apellido',
        'segundo_apellido',
        'sexo',
        'edad',
        'materia'
    ];

    public function alumno() {
        return $this->belongsToMany(Alumno::class)->withPivot('maestro_id', 'alumno_id');
    }
}

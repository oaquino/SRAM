<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $fillable = [
        'nombre',
        'primer_apellido',
        'segundo_apellido',
        'sexo',
        'edad',
        'anio_escolar',
        'nombre_tutor',
        'telefono_tutor'
    ];
    public function maestro() {
        return $this->belongsToMany(Maestro::class)->withPivot('maestro_id', 'alumno_id')->withTimestamps();
    }
}

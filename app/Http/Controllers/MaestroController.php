<?php

namespace App\Http\Controllers;

use App\Models\Maestro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Alert;

class MaestroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maestros = Maestro::get(['id','nombre','primer_apellido', 'segundo_apellido', 'sexo', 'edad','materia']);
        return view('maestros.index')->with(compact('maestros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('maestros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'primer_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'segundo_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'sexo' => 'required|alpha|max:1|min:1',
            'edad' => 'required|integer|between:20,65',
            'materia' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
        ]);
        if( $validator->fails() ) {
            alert()->error('¡Error!', $validator->messages()->all());
            return Redirect::back()->withInput();
        }
        DB::beginTransaction();
        try {
            $maestro = Maestro::create([
                'nombre' => mb_strtoupper($request->nombre),
                'primer_apellido' => mb_strtoupper($request->primer_apellido),
                'segundo_apellido' => mb_strtoupper($request->segundo_apellido),
                'sexo' => mb_strtoupper($request->sexo),
                'edad' => $request->edad,
                'materia' => mb_strtoupper($request->materia)
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            alert()->error('¡Error al guardar!', $e->errorInfo[2]);
            return Redirect::back()->withInput();
        }
        DB::commit();
        alert()->success('Maestro actualizado con éxito','Éxito al guardar');
        return redirect()->route('maestros.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Maestro  $maestro
     * @return \Illuminate\Http\Response
     */
    public function show(Maestro $maestro)
    {
        $maestro = Maestro::find($maestro->id);
        return view('maestros.show')->with(compact('maestro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Maestro  $maestro
     * @return \Illuminate\Http\Response
     */
    public function edit(Maestro $maestro)
    {
        $maestro = Maestro::find($maestro->id);
        return view('maestros.edit')->with(compact('maestro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Maestro  $maestro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maestro $maestro)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'primer_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'segundo_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'sexo' => 'required|alpha|max:1|min:1',
            'edad' => 'required|integer|between:20,65',
            'materia' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
        ]);
        if( $validator->fails() ) {
            alert()->error('¡Error!', $validator->messages()->all());
            return Redirect::back()->withInput();
        }
        DB::beginTransaction();
        try {
            $maestro = Maestro::find($maestro->id);
            $maestro->nombre = mb_strtoupper($request->nombre);
            $maestro->primer_apellido = mb_strtoupper($request->primer_apellido);
            $maestro->segundo_apellido = mb_strtoupper($request->segundo_apellido);
            $maestro->sexo = mb_strtoupper($request->sexo);
            $maestro->edad = $request->edad;
            $maestro->materia = mb_strtoupper($request->materia);
            $maestro->save();
        } catch (\Exception $e) {
        DB::rollback();
        alert()->error('¡Error al guardar!', $e->errorInfo[2]);
        return Redirect::back()->withInput();
    }
    DB::commit();
    alert()->success('Maestro actualizado con éxito','Éxito al guardar');
    return redirect()->route('maestros.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Maestro  $maestro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Maestro $maestro)
    {
        DB::beginTransaction();
        try {
            $ma = Maestro::find($maestro->id);
            $ma->delete();
            $ma->alumno()->detach($maestro->id);
        } catch (\Exception $e) {
            DB::rollback();
            alert()->error('¡Error al guardar!', $e->errorInfo[2]);
            return Redirect::back()->withInput();
        }
        DB::commit();
        alert()->success('Maestro eliminado con éxito');
        return redirect()->route('maestros.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Maestro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DB;
use Alert;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos = Alumno::get(['id','nombre','primer_apellido', 'segundo_apellido','sexo','edad','anio_escolar','nombre_tutor','telefono_tutor']);
        return view('alumnos.index')->with(compact('alumnos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maestros = Maestro::get(['id','materia','nombre','primer_apellido']);
        return view('alumnos.create')->with(compact('maestros'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'primer_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'segundo_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'sexo' => 'required|alpha|max:1|min:1',
            'edad' => 'required|integer|between:3,20',
            'anio_escolar' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
            'nombre_tutor' => 'required|regex:/^[\pL\s\-]+$/u|max:90|min:3',
            'telefono_tutor' => 'required|alpha_num|max:10|min:10'
        ]);
        if( $validator->fails() ) {
            alert()->error('¡Error!', $validator->messages()->all());
            return Redirect::back()->withInput();
        }
        DB::beginTransaction();
        try {
            $alumno = Alumno::create([
                'nombre' => mb_strtoupper($request->nombre),
                'primer_apellido' => mb_strtoupper($request->primer_apellido),
                'segundo_apellido' => mb_strtoupper($request->segundo_apellido),
                'sexo' => mb_strtoupper($request->sexo),
                'edad' => $request->edad,
                'anio_escolar' => mb_strtoupper($request->anio_escolar),
                'nombre_tutor' => mb_strtoupper($request->nombre_tutor),
                'telefono_tutor' => $request->telefono_tutor,
            ]);
            if($request->materia != "" || $request->materia != NULL){
                $alumno->maestro()->attach($request->materia);
            }
        } catch (\Exception $e) {
            alert()->error('¡Error al guardar!', $e->errorInfo[2]);
            DB::rollback();
            return Redirect::back()->withInput();
        }
        DB::commit();
        alert()->success('Alumno almacenado con éxito','Éxito al guardar');
        return redirect()->route('alumnos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show(Alumno $alumno)
    {
        $alumno = Alumno::find($alumno->id);
        $maestros = Maestro::get(['id','materia','nombre','primer_apellido']);
        $asignados= array(); 
        for($i = 0; $i<$alumno->maestro->count(); $i++){
            $asignados[]=$alumno->maestro[$i]->id;
        }
        return view('alumnos.show')->with(compact('alumno','maestros','asignados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumno $alumno)
    {
        $alumno = Alumno::find($alumno->id);
        $asignados= array(); 
        for($i = 0; $i<$alumno->maestro->count(); $i++){
            $asignados[]=$alumno->maestro[$i]->id;
        }
        // dd($asignados);
        $maestros = Maestro::get(['id','materia','nombre','primer_apellido']);
        return view('alumnos.edit')->with(compact('alumno','maestros','asignados'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumno $alumno)
    {
        $validator = Validator::make($request->all(),[
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'primer_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'segundo_apellido' => 'required|regex:/^[\pL\s\-]+$/u|max:30|min:3',
            'sexo' => 'required|alpha|max:1|min:1',
            'edad' => 'required|integer|between:3,20',
            'anio_escolar' => 'required|regex:/^[\pL\s\-]+$/u|max:20|min:3',
            'nombre_tutor' => 'required|regex:/^[\pL\s\-]+$/u|max:90|min:3',
            'telefono_tutor' => 'required|alpha_num|max:10|min:10'
        ]);
        if( $validator->fails() ) {
            alert()->error('¡Error!', $validator->messages()->all());
            return Redirect::back()->withInput();
        }
        DB::beginTransaction();
        try {
            $alumno = Alumno::find($alumno->id);
            $alumno->nombre = mb_strtoupper($request->nombre);
            $alumno->primer_apellido = mb_strtoupper($request->primer_apellido);
            $alumno->segundo_apellido = mb_strtoupper($request->segundo_apellido);
            $alumno->sexo = mb_strtoupper($request->sexo);
            $alumno->edad = $request->edad;
            $alumno->anio_escolar = mb_strtoupper($request->anio_escolar);
            $alumno->nombre_tutor = mb_strtoupper($request->nombre_tutor);
            $alumno->telefono_tutor = $request->telefono_tutor;
            $alumno->save();
            $alumno->maestro()->detach();
            if($request->materia != "" || $request->materia != NULL){
                foreach($request->materia as $materia){
                    $alumno->maestro()->attach($materia);
                }
            }
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            alert()->error('¡Error al guardar!', $e->errorInfo[2]);
            return Redirect::back()->withInput();
        }
        DB::commit();
        alert()->success('Alumno actualizado con éxito','Éxito al guardar');
        return redirect()->route('alumnos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumno $alumno)
    {
        DB::beginTransaction();
        try {
            $al = Alumno::find($alumno->id);
            $al->delete();
            $al->maestro()->detach($alumno->id);
        } catch (\Exception $e) {
            DB::rollback();
            alert()->error('¡Error al guardar!', $e->errorInfo[2]);
            return Redirect::back()->withInput();
        }
        DB::commit();
        alert()->success('Alumno eliminado con éxito');
        return redirect()->route('alumnos.index');
    }
}

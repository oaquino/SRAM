<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',30);
            $table->string('primer_apellido',30);
            $table->string('segundo_apellido',30);
            $table->enum('sexo',['M','F']);
            $table->smallInteger('edad');
            $table->string('anio_escolar');
            $table->string('nombre_tutor',90);
            $table->string('telefono_tutor',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
